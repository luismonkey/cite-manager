import React, { useState } from 'react';
import { StyleSheet, Text, View, TouchableHighlight, TextInput, Button, Alert, ScrollView } from 'react-native';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import shortid from 'shortid';


const Form = ({cites, setCite, setForm, setTextForm}) => {

    const [patient, setPatient] = useState('');
    const [owner, setOwner] = useState('');
    const [phone, setPhone] = useState('');
    const [date, setDate] = useState('');
    const [time, setTime] = useState('');
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const [isTimePickerVisible, setTimePickerVisibility] = useState(false);
    const [symptom, setSymptom] = useState('');

    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };

    const handleConfirmDate = (date) => {
        const options = { year: 'numeric', month: 'long', day: "2-digit" };
        setDate(date.toLocaleDateString(options));
        hideDatePicker();
    };

    //Metodos para controlar el TimePicker

    const showTimePicker = () => {
        setTimePickerVisibility(true);
    };

    const hideTimePicker = () => {
        setTimePickerVisibility(false);
    };

    const handleConfirmTime = (time) => {
        const options = { hour: 'numeric', minute: "2-digit", hour12: true };
        setTime(time.toLocaleTimeString('en-US', options));
        hideTimePicker();
    };

    const newCite = () => {
        
        if(patient.trim() === '' || owner.trim() === '' ||
            phone.trim() === '' || date.trim() === '' || 
            symptom.trim() === '' || time.trim() === ''){

                alertErr();
            }else{
                const cita = { patient, owner, phone, date, time, symptom };
                cita.id = shortid.generate();

                console.log(cita);

                const newCites = [...cites, cita];
                setCite(newCites);
                setForm(false);
                setTextForm('Create Cite')
            }
    };

    const alertErr = () => {
        Alert.alert(
            'Error',
            'All fields are required',
            [{
                text: 'Ok'
            }]
        )
    }

    return (
        <>

            <ScrollView style={styles.formulario}>
                <View>
                    <Text style={styles.label}>Patient:</Text>
                    <TextInput
                        style={styles.input}
                        onChangeText={(texto) => setPatient(texto)}
                    />
                </View>
                <View>
                    <Text style={styles.label}>Owner:</Text>
                    <TextInput
                        style={styles.input}
                        onChangeText={(texto) => setOwner(texto)}
                    />
                </View>
                <View>
                    <Text style={styles.label}>Phone:</Text>
                    <TextInput
                        style={styles.input}
                        onChangeText={(texto) => setPhone(texto)}
                        keyboardType="numeric"
                    />
                </View>

                <View>
                    <Text style={styles.labelPicker}>Date:</Text>
                    <Button color="#107CC4" title="Select Date" onPress={showDatePicker} />
                    <DateTimePickerModal
                        isVisible={isDatePickerVisible}
                        mode="date"
                        onConfirm={handleConfirmDate}
                        onCancel={hideDatePicker}
                    />
                    <Text>{date}</Text>
                </View>

                <View>
                    <Text style={styles.labelPicker}>Hour:</Text>
                    <Button color="#107CC4" title="Select Time" onPress={showTimePicker} />
                    <DateTimePickerModal
                        isVisible={isTimePickerVisible}
                        mode="time"
                        onConfirm={handleConfirmTime}
                        onCancel={hideTimePicker}
                    />
                    <Text>{time}</Text>
                </View>

                <View>
                    <Text style={styles.label}>Symptom:</Text>
                    <TextInput
                        multiline
                        style={styles.input}
                        onChangeText={(texto) => setSymptom(texto)}
                    />
                </View>

                <View>
                    <TouchableHighlight onPress={() => newCite()} style={styles.btnSubmit}>
                        <Text style={styles.txtSubmit}>Create Cite</Text>
                    </TouchableHighlight>
                </View>
            </ScrollView>

        </>
    );
}

const styles = StyleSheet.create({
    formulario: {
        backgroundColor: '#FFF',
        paddingHorizontal: 20,
        paddingVertical: 10,
        marginVertical: 10,
        marginBottom: 20

    },
    label: {
        fontWeight: 'bold',
        fontSize: 18,
        marginTop: 10
    },
    input: {
        marginTop: 10,
        height: 30,
        borderColor: '#B7BDD5',
        borderWidth: 1,
        borderStyle: 'solid',
        paddingHorizontal: 5
    },
    labelPicker: {
        marginBottom: 10,
        fontWeight: 'bold',
        fontSize: 18,
        marginTop: 10
    },
    btnSubmit: {
        backgroundColor: '#107CC4',
        padding: 10,
        marginVertical: 10
    },
    txtSubmit: {
        textAlign: 'center',
        fontWeight: 'bold',
        color: '#fff'
    },
    btnPicker: {
        color: '#107CC4'
    }
})

export default Form;