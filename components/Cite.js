import React from 'react';
import { StyleSheet, Text, View, TouchableHighlight } from 'react-native';

const Cite = ({ item, deleteCita }) => { 

    const deleteCite = id => {
        console.log('delete...', id);
        deleteCita(id)
    }
    
    return (
        <View style={styles.cite}>
            <View>
                <Text style={styles.label}>Patient: </Text>
                <Text style={styles.text}>{item.patient}</Text>
            </View>
            <View>
                <Text style={styles.label}>Owner: </Text>
                <Text style={styles.text}>{item.owner}</Text>
            </View>
            <View>
                <Text style={styles.label}>Symptom: </Text>
                <Text style={styles.text}>{item.symptom}</Text>
            </View>

            <View>
                <TouchableHighlight onPress={ () => deleteCite(item.id) } style={styles.btnDelete}>
                    <Text style={styles.txtDelete}>Delete</Text>
                </TouchableHighlight>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    cite: {
        backgroundColor: '#fff',
        borderBottomColor: '#B7BDD5',
        borderStyle: 'solid',
        borderBottomWidth: 1,
        paddingVertical: 20,
        paddingHorizontal: 10, 
    },
    label: {
        fontWeight: 'bold',
        fontSize: 18,
        marginTop: 10
    },
    text: {
        fontSize: 17
    },
    btnDelete: {
        backgroundColor: 'red',
        padding: 10,
        marginVertical: 10
    },
    txtDelete: {
        textAlign: 'center',
        fontWeight: 'bold',
        color: '#fff'
    }
})

export default Cite;