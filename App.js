import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { StyleSheet, Text, View, FlatList, TouchableHighlight, TouchableWithoutFeedback, Keyboard } from 'react-native';
import Cite from './components/Cite';
import Form from './components/Form';

export default function App() {

  const [form, setForm] = useState(false);
  const [textForm, setTextForm] = useState('Create Cite')
  const [cites, setCite] = useState([
    { id: "1", owner: "Example1", patient: "Luis", symptom: "does not eat" },
    { id: "2", owner: "Example2", patient: "Carla", symptom: "does not sleep" },
    { id: "3", owner: "Example3", patient: "Belen", symptom: "does not sing" }
  ])

  const deleteCita = id => {
    setCite((citasActuales) => {
      return citasActuales.filter(cita => cita.id !== id);
    })
  };

  const showForm = () => {
    setForm(!form);
    if (textForm === 'Create Cite') {
      setTextForm('View Cites')
    } else {
      setTextForm('Create Cite')
    }
  };

  //medoto para cerrar el teclado 

  const closeKeyboard = () => {
    Keyboard.dismiss();
  }

  return (
    <TouchableWithoutFeedback onPress={() => closeKeyboard()}>
      <View style={styles.container}>
        <Text style={styles.title}>Cite Manager</Text>

        <View>
          <TouchableHighlight onPress={() => showForm()} style={styles.btnShowForm}>
            <Text style={styles.txtShowForm}>{textForm}</Text>
          </TouchableHighlight>
        </View>
        <View style={styles.cont}>
          {form ? (
            <Form cites={cites} setCite={setCite} setForm={setForm} setTextForm={setTextForm} />
          ) : (
              <>
                {cites.length == 0 && <Text style={styles.title}>no appointments, add one</Text>}
                <FlatList
                  style={styles.list}
                  data={cites}
                  renderItem={({ item }) => <Cite item={item} deleteCita={deleteCita} />}
                />
              </>
            )}


        </View>
      </View>
    </TouchableWithoutFeedback>

  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#3498DB',
    flex: 1
  },
  title: {
    marginTop: 25,
    textAlign: 'center',
    color: '#fff',
    fontSize: 24,
    fontWeight: 'bold'
  },
  cont: {
    flex: 1,
    marginHorizontal: '2.5%',
  },
  list: {
    flex: 1
  },
  txtShowForm: {
    textAlign: 'center',
    fontWeight: 'bold',
    color: '#fff'
  },
  btnShowForm: {
    backgroundColor: '#107CC4',
    padding: 10,
    marginVertical: 10
  }
});
